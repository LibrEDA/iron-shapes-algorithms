<!--
SPDX-FileCopyrightText: 2022 Thomas Kramer

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Geometric algorithms for iron-shapes

This create contains geometric algorithms for the [iron-shapes](https://codeberg.org/libreda/iron-shapes) crate. Mostly this are non-trivial
algorithms and hence would make the the basic geometry library too complicated.
