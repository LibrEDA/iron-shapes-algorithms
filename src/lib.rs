// SPDX-FileCopyrightText: 2022 Thomas Kramer
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! This create contains geometric algorithms for the [iron-shapes](https://codeberg.org/libreda/iron-shapes) crate.
//! Mostly this are non-trivial algorithms and hence would make the the basic geometry library too complicated.
//!
//! # Boolean operations
//! Boolean operations on polygons are complicated enough to be implemented in a separate crate.
//! Therefore this crate re-exports [iron-shapes-booleanop](https://codeberg.org/libreda/iron-shapes-booleanop) as `booleanop`.

#![deny(missing_docs)]

pub mod prelude;

pub mod rectangle_decomposition;

// Re-export booleanops
pub use iron_shapes_booleanop as booleanop;
