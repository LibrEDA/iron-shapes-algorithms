// SPDX-FileCopyrightText: 2022 Thomas Kramer
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Short-cut to import all functionality of this library.

pub use super::rectangle_decomposition::*;
pub use iron_shapes::prelude::*;
